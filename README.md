## Task 
Create a service, which returns product recommendations depending on current weather.

#### Usage: 
* GET host.com/api/products/recommended/:city
* {city} - any LT city without LT characters. Use dashes instead of spaces.


#### Deployed at:
https://recommended-products.herokuapp.com/api/products/recommended/palanga

#### Tech:
* PHP, Laravel
* MySql

#### Local setup:
* setup laravel/homestead 
* composer install
* php artisan migrate
* php artisan db:seed


